/**
 * Created by samdesmedt on 10/07/2017.
 */
$(document).ready(function() {
  $('html, body').animate({
    scrollTop: $("#content").offset().top
  }, 600);



  $("#arrowIconScroll").on("click", function( e ) {

    e.preventDefault();

    $('html, body').animate({
      scrollTop: $("#content").offset().top
    }, 600);
  });





  $('#northfreezeNVbtn').click(function(){

    $('#northfreezeNV').slideToggle();
    $('#northfreezeNVbtn').css("visibility","hidden")
  });

  $('#northfreezeNVcloseBtn').click(function(){

    $('#northfreezeNV').slideUp(400, function() {

      $('#northfreezeNVbtn').css("visibility","visible")
    });

  });






  $('#nortrafficNVbtn').click(function(){

    $('#nortrafficNV').slideToggle();
    $('#nortrafficNVbtn').css("visibility","hidden")
  });

  $('#nortrafficNVCloseBtn').click(function(){

    $('#nortrafficNV').slideUp(
      400, function() {

        $('#nortrafficNVbtn').css("visibility","visible")
      }
    );
  });




  $('#nortrafficFSbtn').click(function(){

    $('#nortrafficFS').slideToggle();
    $('#nortrafficFSbtn').css("visibility","hidden")
  });

  $('#nortrafficFSCloseBtn').click(function(){

    $('#nortrafficFS').slideUp( 400, function() {

      $('#nortrafficFSbtn').css("visibility","visible")
    });
  });




  $('#northLinkNVbtn').click(function(){

    $('#northLinkNV').slideToggle();
    $('#northLinkNVbtn').css("visibility","hidden")
  });

  $('#northLinkNVCloseBtn').click(function(){

    $('#northLinkNV').slideUp( 400, function() {

      $('#northLinkNVbtn').css("visibility","visible")
    });
  });




  $('#btnSideBar').on('click touchstart', function(e){
    $('html').toggleClass('menu-active');
    $(this).find('i').toggleClass('fa-bars fa-times');
    e.preventDefault();
  });







});